from django.shortcuts import render, redirect
from django.http import  HttpResponse
from .models import Tienda
from .forms import TiendaForm
# Create your views here.

def inicio(request): 
    return render(request, 'paginas/inicio.html')

def nosotros(request): 
    return render(request, 'paginas/nosotros.html')

def tienda(request): 
    tienda = Tienda.objects.all()
    return render(request, 'tienda/index.html', {'tienda': tienda})

def crear(request):
    formulario = TiendaForm(request.POST or None, request.FILES or None)
    if formulario.is_valid():
        formulario.save()
        return redirect('tienda')
    return render(request, 'tienda/crear.html', {'formulario': formulario})

def editar(request, id):
    tienda = Tienda.objects.get(id=id)
    formulario = TiendaForm(request.POST or None, request.FILES or None, instance=tienda)
    if formulario.is_valid() and request.POST:
        formulario.save()
        return redirect('tienda')
    return render(request, 'tienda/editar.html', {'formulario':formulario})

def eliminar(request, id):
    tienda = Tienda.objects.get(id=id)
    tienda.delete()
    return redirect('tienda')
    
