# ***************  sentencias IF ******************


#compuertas logicas AND OR != ==
# print(5!=5 and 6==6)
# print(5!=5 or 6==6)

#cilos for , while

# with open("./src/trp1_sesiones/static/txt/sesion2.txt", "r") as archivo:
#     lineas = archivo.readlines()
#     conjunto = lineas[2].split(";")
#     # if lineas[2]
#     #Leer archivos
#     cont=0
#     for frase in conjunto:
#         if frase == "Leer archivos planos":
#             print("columna {}".format(cont))  # Imprime la tercera línea del archivo [0],[1]
#         cont=cont+1

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# datos = "8;31/08/2023;Workshop;Taller práctico de integración de lo aprendido;Desarrollo de un proyecto pequeño"


# with open("./src/trp1_sesiones/static/txt/sesion2.txt", "r") as archivo:
#     lineas = archivo.readlines()
#     cont=len(lineas)
#     datos_dos = "{}{}".format(cont,datos[1:])

# with open("./src/trp1_sesiones/static/txt/sesion2.txt", "a") as archivo:
#     archivo.write("\n"+datos_dos)

#***************************************************************************        
#importancion de librerias
import pandas as pd

#lectura de archivos 

df_archivo = pd.read_csv("./src/trp1_sesiones/static/txt/sesion2.txt",sep=";",encoding='utf8')
df_archivo["referencias"] = "https://pandas.pydata.org/docs/user_guide/index.html#user-guide"
df_archivo.to_excel("E:/taller_programacion1/andres_callejas05082/src/trp1_sesiones/static/xlsx/sesiones_clase.xlsx",index=False)
 #E:\taller_programacion1\andres_callejas05082\src\trp1_sesiones\static\xlsx

