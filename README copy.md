# Taller de Programación 1

Bienvenidos al repositorio oficial del curso "Taller de Programación 1". Aquí encontrarán todos los recursos, fechas y temarios relacionados con el curso.

## Fechas de Evaluación

1. **Entregable 1**: 7 de Septiembre, 2023
2. **Entregable 2**: 28 de Septiembre, 2023
3. **Parcial 1**: 5 Octubre, 2023
4. **Entregable 3**: 2 Noviembre, 2023
5. **Entregable 4**: 30 Noviembre, 2023
6. **Parcial Final**: 7 Diciembre, 2023

## Herramientas Utilizadas

- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/) Extenciones (Python , Git grafh , Git lens, jupyter, Bracket Pair Color, icon material, **Excel Viewer**)
- n , Git grafh , Git lens)
- **Python 3.9.2**: [Python descarga](https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)

## Control de Versiones

- **git clone** `<url>`
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

- **python -m venv venv** creacion de espacio de trabajo
- **.\venv\Scripts\activate** activar entorno virtual

## Comandos VSCode

- Control + ñ Terminal

## librerias
   instalación de librerias : pip install
   listar librerias :         pip list

   importar librerias : import `<nombre_libreria> as <alias_libreria>`

- pandas https://pandas.pydata.org/docs/user_guide/index.html#user-guide
- openpyxl - guardar archivos de excel
- actualizar pip - python.exe -m pip install --upgrade pip
- jupyter - libreria que levanta cuadernos en python
### 1. Comandos de PIP

| item | comando      | Documentación y detalle                                                                                                                      |
| ------- | ---------- | ------------------------------------------------------------------------------------------------------------------------- |
| 1       | pip install | base inicial para instalar cualquier libreria comando completo <pip install nombre libreria>                     |
| 2       | pip list | lista las libreria instaladas|
| 3       | jupyter notebook | lanza una interfaz web para trabajar con cuadernos de jupyter|

### 2. librerias trabajadas
| item | libreria      | Documentación      |
| ------- | ---------- | ------------------------------------------------------------------------------------------------------------------------- |
| 1       | pandas| libreria para trabajar con series y dataframe , y procesar diferentes formatos de archivos url: https://pandas.pydata.org/docs/user_guide/index.html#user-guide |
| 2       | openpyxl | libreria para la lectura de archivos de excel|
| 3       | lxml | requerida para trabajar con la funcion de pandas read_html|
| 2       | jupyter | libreria para levantar cuadernos en python|

## Sesiones

| Sesión | Fecha      | Tema                                                                                                                      |
| ------- | ---------- | ------------------------------------------------------------------------------------------------------------------------- |
| 1       | 10/08/2023 | Introducción a la programación                                                                                          |
| 1       | 10/08/2023 | Control de versiones                                                                                                      |
| 2       | 16/08/2023 | Variables y Tipos de Variables, estructura de datos, lectura y escritura de archivos ".src\trp1_sesiones\tp1_sesion_2.py" |
| 3       | 17/08/2023 | lectura y escritura de archivos planos ".src\trp1_sesiones\tp1_sesion_3.py"                                               |
| 4     | 23/08/2023 | lectura y escritura csv y xlsx  |
| 5     | 24/08/2023 | pandas dataframe urls                                             |
