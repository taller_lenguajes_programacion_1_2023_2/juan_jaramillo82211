class Tienda: 

    def __init__(self, id_tienda = 0, nombre_t="",direccion_t="",gerente_t="",tamano_t="",telefono_t="",email_t="" ):
        
        
        self.__id_tienda = id_tienda
        self.__nombre_t=nombre_t
        self.__direccion_t=direccion_t
        self.__gerente_t=gerente_t
        self.__tamano_t=tamano_t
        self.__telefono_t=telefono_t
        self.__email_t=email_t

       # Getter y setter para id_tienda
    @property
    def id_tienda(self):
        return self.__id_tienda

    @id_tienda.setter
    def id_tienda(self, nuevo_id):
        self.__id_tienda = nuevo_id

    # Getter y setter para nombre_t
    @property
    def nombre_t(self):
        return self.__nombre_t

    @nombre_t.setter
    def nombre_t(self, nuevo_nombre):
        self.__nombre_t = nuevo_nombre

    # Getter y setter para direccion_t
    @property
    def direccion_t(self):
        return self.__direccion_t

    @direccion_t.setter
    def direccion_t(self, nueva_direccion):
        self.__direccion_t = nueva_direccion

    # Getter y setter para gerente_t
    @property
    def gerente_t(self):
        return self.__gerente_t

    @gerente_t.setter
    def gerente_t(self, nuevo_gerente):
        self.__gerente_t = nuevo_gerente

    # Getter y setter para tamano_t
    @property
    def tamano_t(self):
        return self.__tamano_t

    @tamano_t.setter
    def tamano_t(self, nuevo_tamano):
        self.__tamano_t = nuevo_tamano

    # Getter y setter para telefono_t
    @property
    def telefono_t(self):
        return self.__telefono_t

    @telefono_t.setter
    def telefono_t(self, nuevo_telefono):
        self.__telefono_t = nuevo_telefono

    # Getter y setter para email_t
    @property
    def email_t(self):
        return self.__email_t

    @email_t.setter
    def email_t(self, nuevo_email):
        self.__email_t = nuevo_email

def __str__(self):
    return f"Tienda: {self.nombre_t}, Dirección: {self.direccion_t}, Gerente: {self.gerente_t}, Tamaño: {self.tamano_t}, Teléfono: {self.telefono_t}, Email: {self.email_t}"

class Prenda: 
    def __init__(self, id_prenda = 0, tipo_p="",marca_p="",tamano_p="",color_p="",material_p="", precio_p=0.0 ):

        self.__id_prenda = id_prenda
        self.__tipo_p=tipo_p
        self.__marca_p=marca_p
        self.__tamano_p=tamano_p
        self.__color_p=color_p
        self.__material_p=material_p
        self.__precio_p=precio_p

        
    # Getter y setter para id_prenda
    @property
    def id_prenda(self):
        return self.__id_prenda

    @id_prenda.setter
    def id_prenda(self, nuevo_id):
        self.__id_prenda = nuevo_id

    # Getter y setter para tipo_p
    @property
    def tipo_p(self):
        return self.__tipo_p

    @tipo_p.setter
    def tipo_p(self, nuevo_tipo):
        self.__tipo_p = nuevo_tipo

    # Getter y setter para marca_p
    @property
    def marca_p(self):
        return self.__marca_p

    @marca_p.setter
    def marca_p(self, nueva_marca):
        self.__marca_p = nueva_marca

    # Getter y setter para tamano_p
    @property
    def tamano_p(self):
        return self.__tamano_p

    @tamano_p.setter
    def tamano_p(self, nuevo_tamano):
        self.__tamano_p = nuevo_tamano

    # Getter y setter para color_p
    @property
    def color_p(self):
        return self.__color_p

    @color_p.setter
    def color_p(self, nuevo_color):
        self.__color_p = nuevo_color

    # Getter y setter para material_p
    @property
    def material_p(self):
        return self.__material_p

    @material_p.setter
    def material_p(self, nuevo_material):
        self.__material_p = nuevo_material

    # Getter y setter para precio_p
    @property
    def precio_p(self):
        return self.__precio_p

    @precio_p.setter
    def precio_p(self, nuevo_precio):
        self.__precio_p = nuevo_precio

def __str__(self):
    return f"Prenda ({self.tipo_p}): {self.marca_p}, Tamaño: {self.tamano_p}, Color: {self.color_p}, Material: {self.material_p}, Precio: {self.precio_p}"


class Cliente: 
    def __init__(self, nombre_c = "", apellido_c="",cedula_c="",telefono_c="",talla_c="",email_c="" ):

        self.__nombre_c = nombre_c
        self.__apellido_c=apellido_c
        self.__cedula_c=cedula_c
        self.__telefono_c=telefono_c
        self.__talla_c=talla_c
        self.__email_c=email_c
        
        
    # Getter y setter para nombre_c
    @property
    def nombre_c(self):
        return self.__nombre_c

    @nombre_c.setter
    def nombre_c(self, nuevo_nombre):
        self.__nombre_c = nuevo_nombre

    # Getter y setter para apellido_c
    @property
    def apellido_c(self):
        return self.__apellido_c

    @apellido_c.setter
    def apellido_c(self, nuevo_apellido):
        self.__apellido_c = nuevo_apellido

    # Getter y setter para cedula_c
    @property
    def cedula_c(self):
        return self.__cedula_c

    @cedula_c.setter
    def cedula_c(self, nueva_cedula):
        self.__cedula_c = nueva_cedula

    # Getter y setter para telefono_c
    @property
    def telefono_c(self):
        return self.__telefono_c

    @telefono_c.setter
    def telefono_c(self, nuevo_telefono):
        self.__telefono_c = nuevo_telefono

    # Getter y setter para talla_c
    @property
    def talla_c(self):
        return self.__talla_c

    @talla_c.setter
    def talla_c(self, nueva_talla):
        self.__talla_c = nueva_talla

    # Getter y setter para email_c
    @property
    def email_c(self):
        return self.__email_c

    @email_c.setter
    def email_c(self, nuevo_email):
        self.__email_c = nuevo_email

def __str__(self):
    return f"Cliente: {self.nombre_c} {self.apellido_c}, Cédula: {self.cedula_c}, Teléfono: {self.telefono_c}, Talla: {self.talla_c}, Email: {self.email_c}"


class Compra: 
    def __init__(self, id_compra = "", fechaCompra="", Tienda=Tienda,Cliente=Cliente,cantidad_compras=0 ):

        self.__id_compra = id_compra
        self.__fechaCompra=fechaCompra
        self.__Tienda=Tienda
        self.__Cliente=Cliente
        self.__cantidad_compras=cantidad_compras
      
      
    @property
    def id_compra(self):
        return self.__id_compra

    @id_compra.setter
    def id_compra(self, nuevo_id_compra):
        self.__id_compra = nuevo_id_compra

    @property
    def fechaCompra(self):
        return self.__fechaCompra

    @fechaCompra.setter
    def fechaCompra(self, nueva_fechaCompra):
        self.__fechaCompra = nueva_fechaCompra

    @property
    def Tienda(self):
        return self.__Tienda

    @Tienda.setter
    def Tienda(self, nueva_tienda):
        self.__Tienda = nueva_tienda

    @property
    def Cliente(self):
        return self.__Cliente

    @Cliente.setter
    def Cliente(self, nuevo_cliente):
        self.__Cliente = nuevo_cliente

    @property
    def cantidad_compras(self):
        return self.__cantidad_compras

    @cantidad_compras.setter
    def cantidad_compras(self, nueva_cantidad_compras):
        self.__cantidad_compras = nueva_cantidad_compras

def __str__(self):
    return f"Compra: ID {self.id_compra}, Fecha: {self.fechaCompra}\nTienda: {self.Tienda}\nCliente: {self.Cliente}\nCantidad de Compras: {self.cantidad_compras}"










