import sqlite3

# Conexión a la base de datos (crea una nueva si no existe)
conn = sqlite3.connect('tienda.db')

# Crear un cursor para ejecutar comandos SQL
cursor = conn.cursor()

# Crear tabla Cliente
cursor.execute('''CREATE TABLE Cliente (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nombre TEXT,
                    apellido TEXT,
                    cedula TEXT,
                    telefono TEXT,
                    talla TEXT,
                    email TEXT
                )''')

# Crear tabla Tienda
cursor.execute('''CREATE TABLE Tienda (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nombre TEXT,
                    direccion TEXT,
                    gerente TEXT,
                    tamano TEXT,
                    telefono TEXT,
                    email TEXT
                )''')

# Crear tabla Prenda
cursor.execute('''CREATE TABLE Prenda (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    tipo TEXT,
                    marca TEXT,
                    tamano TEXT,
                    color TEXT,
                    material TEXT,
                    precio REAL
                )''')

# Crear tabla Compra con referencias a Cliente y Tienda
cursor.execute('''CREATE TABLE Compra (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    fechaCompra TEXT,
                    Cliente_id INTEGER,
                    Tienda_id INTEGER,
                    cantidad_compras INTEGER,
                    FOREIGN KEY (Cliente_id) REFERENCES Cliente (id),
                    FOREIGN KEY (Tienda_id) REFERENCES Tienda (id)
                )''')

# Guardar cambios y cerrar la conexión
conn.commit()
conn.close()
