from conexion import Conexion

class Empresa(Conexion):
    def __init__(self, nombre_empresa="", industria=""):
        super().__init()
        self.id_empresa = None
        self.nombre_empresa = nombre_empresa
        self.industria = industria

    def guardar(self):
        query = """
        INSERT INTO empresa (nombre_empresa, industria)
        VALUES (?, ?)
        """
        values = (self.nombre_empresa, self.industria)

        try:
            self.cursor.execute(query, values)
            self.conn.commit()
        except Exception as error:
            print(error)

    def editar(self, id_empresa=""):
        query = """
        UPDATE empresa
        SET nombre_empresa = ?, industria = ?
        WHERE id_empresa = ?
        """
        values = (self.nombre_empresa, self.industria, self.id_empresa)

        try:
            self.cursor.execute(query, values)
            self.conn.commit()
        except Exception as error:
            print(error)

    def eliminar(self, id_empresa=""):
        if id_empresa != "":
            query = """
            DELETE FROM empresa WHERE id_empresa = ?
            """
            values = (id_empresa,)

            try:
                self.cursor.execute(query, values)
                self.conn.commit()
            except Exception as error:
                print(error)
        else:
            print("Error: No se proporcionó un ID de empresa para eliminar")

    def listar(self):
        query = """
        SELECT * FROM empresa
        """
        lista = []

        try:
            self.cursor.execute(query)
            lista = self.cursor.fetchall()
        except Exception as error:
            print(error)

        return lista

    def __str__(self):
        return "Empresa: {}, Industria: {}".format(self.nombre_empresa, self.industria)
