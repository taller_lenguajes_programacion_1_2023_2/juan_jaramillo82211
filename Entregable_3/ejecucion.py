import tkinter as tk
from interfaz import Frame

def main():
    root = tk.Tk()
    root.title('Gestión de Empresas y Empleados')
    root.resizable(False, False)
    app = Frame(root)
    app.pack()
    root.mainloop()

if __name__ == '__main__':
    main()
