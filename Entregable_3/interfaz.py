import tkinter as tk
from tkinter import ttk, messagebox
from empresa import Empresa, Empleado

class Frame(tk.Frame):
    def __init__(self, root=None):
        super().__init__(root, width=600, height=400)
        self.root = root
        self.pack()
        self.id_empresa = None
        self.id_empleado = None
        self.empresa = Empresa()
        self.empleado = Empleado()

        self.campos_empresa()
        self.campos_empleado()
        self.tabla_empresas()
        self.tabla_empleados()

    def campos_empresa(self):
        self.lbl_nombre_empresa = tk.Label(self, text='Nombre de la Empresa:')
        self.lbl_nombre_empresa.grid(row=0, column=0, padx=10, pady=5)
        self.var_nombre_empresa = tk.StringVar()
        self.input_nombre_empresa = tk.Entry(self, textvariable=self.var_nombre_empresa)
        self.input_nombre_empresa.grid(row=0, column=1, padx=10, pady=5)

        self.lbl_industria = tk.Label(self, text='Industria:')
        self.lbl_industria.grid(row=1, column=0, padx=10, pady=5)
        self.var_industria = tk.StringVar()
        self.input_industria = tk.Entry(self, textvariable=self.var_industria)
        self.input_industria.grid(row=1, column=1, padx=10, pady=5)

        self.btn_guardar_empresa = tk.Button(self, text="Guardar Empresa", command=self.guardar_empresa)
        self.btn_guardar_empresa.grid(row=2, column=0, columnspan=2, padx=10, pady=10)

    def campos_empleado(self):
        self.lbl_nombre_empleado = tk.Label(self, text='Nombre del Empleado:')
        self.lbl_nombre_empleado.grid(row=0, column=2, padx=10, pady=5)
        self.var_nombre_empleado = tk.StringVar()
        self.input_nombre_empleado = tk.Entry(self, textvariable=self.var_nombre_empleado)
        self.input_nombre_empleado.grid(row=0, column=3, padx=10, pady=5)

        self.lbl_empresa = tk.Label(self, text='ID de la Empresa:')
        self.lbl_empresa.grid(row=1, column=2, padx=10, pady=5)
        self.var_empresa = tk.StringVar()
        self.input_empresa = tk.Entry(self, textvariable=self.var_empresa)
        self.input_empresa.grid(row=1, column=3, padx=10, pady=5)

        self.btn_guardar_empleado = tk.Button(self, text="Guardar Empleado", command=self.guardar_empleado)
        self.btn_guardar_empleado.grid(row=2, column=2, columnspan=2, padx=10, pady=10)

    def guardar_empresa(self):
        self.empresa.nombre_empresa = self.var_nombre_empresa.get()
        self.empresa.industria = self.var_industria.get()

        if self.id_empresa is None:
            self.empresa.guardar()
        else:
            self.empresa.editar(self.id_empresa)

        self.tabla_empresas()
        self.limpiar_campos_empresa()

    def guardar_empleado(self):
        self.empleado.nombre_empleado = self.var_nombre_empleado.get()
        self.empleado.id_empresa = self.var_empresa.get()

        if self.id_empleado is None:
            self.empleado.guardar()
        else:
            self.empleado.editar(self.id_empleado)

        self.tabla_empleados()
        self.limpiar_campos_empleado()

    def tabla_empresas(self):
        pass

    def tabla_empleados(self):
        pass

    def limpiar_campos_empresa(self):
        self.id_empresa = None
        self.var_nombre_empresa.set('')
        self.var_industria.set('')

    def limpiar_campos_empleado(self):
        self.id_empleado = None
        self.var_nombre_empleado.set('')
        self.var_empresa.set('')

if __name__ == "__main__":
    root = tk.Tk()
    app = Frame(root)
    app.mainloop()
