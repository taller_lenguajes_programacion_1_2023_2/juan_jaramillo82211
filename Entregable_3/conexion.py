import sqlite3
from tkinter import messagebox

class Conexion:
    def __init__(self):
        self.ruta_db = 'C:\Taller_lenguajes1\repositorio\juan_jaramillo82211'
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        #self.crear_tabla()
    
    def crear_tabla(self):
        query = """
        CREATE TABLE IF NOT EXISTS empresa (
            id_empresa INTEGER PRIMARY KEY AUTOINCREMENT,
            nombre_empresa VARCHAR (100), 
            industria VARCHAR (100)
        )
        """
        try:
            self.cursor.execute(query)
            self.cerrar()
            messagebox.showinfo("INFO : Creación Tabla Exitosa", "Se creó la tabla correctamente")
        except Exception as error:
            print(error)
            messagebox.showerror("ERROR: Creación Tabla No Exitosa", "No se pudo crear la tabla")

    def borrar_tabla(self):
        query = """
        DROP TABLE IF EXISTS empresa
        """
        try:
            self.cursor.execute(query)
            self.cerrar()
            messagebox.showinfo("INFO : Borrado Tabla Exitosa", "Se eliminó la tabla correctamente")
        except Exception as error:
            print(error)
            messagebox.showerror("ERROR: Borrado Tabla No Exitosa", "No se pudo eliminar la tabla")
    
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
