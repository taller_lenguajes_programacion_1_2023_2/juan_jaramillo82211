import sqlite3

# Clase para gestionar la conexión a la base de datos
class Conexion:
    def __init__(self, database_name):
        self.conn = sqlite3.connect(database_name)
        self.cursor = self.conn.cursor()

    def close_connection(self):
        self.conn.close()

# Clase para gestionar la tabla de empresas
class Empresa:
    def __init__(self, nombre_empresa, industria):
        self.nombre_empresa = nombre_empresa
        self.industria = industria

    def guardar(self):
        query = 'INSERT INTO empresas (nombre_empresa, industria) VALUES (?, ?)'
        values = (self.nombre_empresa, self.industria)

        try:
            conexion = Conexion('empresa.db')
            conexion.cursor.execute(query, values)
            conexion.conn.commit()
            conexion.close_connection()
        except Exception as error:
            print(error)

    @staticmethod
    def listar_empresas():
        query = 'SELECT * FROM empresas'
        try:
            conexion = Conexion('empresa.db')
            conexion.cursor.execute(query)
            empresas = conexion.cursor.fetchall()
            conexion.close_connection()
            return empresas
        except Exception as error:
            print(error)
            return []

# Clase para gestionar la tabla de empleados
class Empleado:
    def __init__(self, nombre_empleado, id_empresa):
        self.nombre_empleado = nombre_empleado
        self.id_empresa = id_empresa

    def guardar(self):
        query = 'INSERT INTO empleados (nombre_empleado, id_empresa) VALUES (?, ?)'
        values = (self.nombre_empleado, self.id_empresa)

        try:
            conexion = Conexion('empresa.db')
            conexion.cursor.execute(query, values)
            conexion.conn.commit()
            conexion.close_connection()
        except Exception as error:
            print(error)

    @staticmethod
    def listar_empleados():
        query = 'SELECT * FROM empleados'
        try:
            conexion = Conexion('empresa.db')
            conexion.cursor.execute(query)
            empleados = conexion.cursor.fetchall()
            conexion.close_connection()
            return empleados
        except Exception as error:
            print(error)
            return []

if __name__ == "__main__":
    # Crear tablas si no existen
    conexion = Conexion('empresa.db')
    conexion.cursor.execute('''
    CREATE TABLE IF NOT EXISTS empresas (
        id INTEGER PRIMARY KEY,
        nombre_empresa TEXT,
        industria TEXT
    )
    ''')
    conexion.cursor.execute('''
    CREATE TABLE IF NOT EXISTS empleados (
        id INTEGER PRIMARY KEY,
        nombre_empleado TEXT,
        id_empresa INTEGER,
        FOREIGN KEY (id_empresa) REFERENCES empresas (id)
    )
    ''')
    conexion.close_connection()

    # Ejemplos de uso
    empresa1 = Empresa("Empresa A", "Industria X")
    empresa1.guardar()
    empleado1 = Empleado("Empleado 1", 1)
    empleado1.guardar()

    print("Empresas:")
    empresas = Empresa.listar_empresas()
    for empresa in empresas:
        print(empresa)

    print("Empleados:")
    empleados = Empleado.listar_empleados()
    for empleado in empleados:
        print(empleado)
